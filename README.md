# phone_dictionary

# dictionary
Maps a phone number to a bunch of strings using a predefined dictionary

* Ruby version - 2.6.0

# How to run

1. clone the repository using command 
git clone https://neelima_joshi93@bitbucket.org/neelima_joshi93/phone_dictionary.git

2. Navigate inside the directory 'phone_dictionary' and run the following file

    - ruby main.rb
    - A prompt will ask you 'Enter your input'
    - Enter a 10(configurable) digit phone number (no 0's/1's/special characters) and hit enter
    - run command 'rspec' to run the tests

    (don't worry validation is handled in case you enter invalid input!!)

# What to expect

An array is returned consisting of valid strings in case there are any.
An empty array is returned in case there is no valid strings for the input phone number.
String with a validation  message is returned in case of invalid input.

INPUT: 6686787825

OUTPUT: *["mot,opts,taj", "mot,opus,taj", "mot,orts,taj", "not,opts,taj", "not,opus,taj", "not,orts,taj", "oot,opts,taj", "oot,opus,taj", "oot,orts,taj", "mot,opt,puck", "mot,opt,ruck", "mot,opt,suck", "mot,ort,puck", "mot,ort,ruck", "mot,ort,suck", "not,opt,puck", "not,opt,ruck", "not,opt,suck", "not,ort,puck", "not,ort,ruck", "not,ort,suck", "oot,opt,puck", "oot,opt,ruck", "oot,opt,suck", "oot,ort,puck", "oot,ort,ruck", "oot,ort,suck", "noun,struck", "onto,struck", "noun,pup,taj", "noun,pur,taj", "noun,pus,taj", "noun,sup,taj", "noun,suq,taj", "onto,pup,taj", "onto,pur,taj", "onto,pus,taj", "onto,sup,taj", "onto,suq,taj", "motor,truck", "motor,usual", "nouns,truck", "nouns,usual", "motortruck"]*

INPUT: 2282668687

OUTPUT: *["act,amounts", "act,contour", "bat,amounts", "bat,contour", "cat,amounts", "cat,contour", "act,boot,mus", "act,boot,nus", "act,boot,our", "act,coot,mus", "act,coot,nus", "act,coot,our", "bat,boot,mus", "bat,boot,nus", "bat,boot,our", "bat,coot,mus", "bat,coot,nus", "bat,coot,our", "cat,boot,mus", "cat,boot,nus", "cat,boot,our", "cat,coot,mus", "cat,coot,nus", "cat,coot,our", "act,boo,tots", "act,boo,tour", "act,con,tots", "act,con,tour", "act,coo,tots", "act,coo,tour", "bat,boo,tots", "bat,boo,tour", "bat,con,tots", "bat,con,tour", "bat,coo,tots", "bat,coo,tour", "cat,boo,tots", "cat,boo,tour", "cat,con,tots", "cat,con,tour", "cat,coo,tots", "cat,coo,tour", "acta,mounts", "acta,mot,mus", "acta,mot,nus", "acta,mot,our", "acta,not,mus", "acta,not,nus", "acta,not,our", "acta,oot,mus", "acta,oot,nus", "acta,oot,our", "catamounts"]*


INPUT: 9999999999
  OUTPUT: *[]*

INPUT: 7836119750

OUTPUT: *Input size should be 10*

*Input should only contain numbers ( 0's and 1's are not allowed)*


# Approach

1. Create a hash from the provided dictionary.txt wherein the values are the words in the dictionary with their mapped integer value as keys.

2. Generate all different combinations in which a 10 digit string can be divided in such a way that each partition should have atleast three(configurable) numbers. For a 10(configurable) digit number combination are .

PARTITIONS = [[3, 7], [3, 4, 3], [3, 3, 4], [4, 6], [4, 3, 3], [5, 5], [6, 4], [7, 3], [10]]

3. For each partition repeat the following steps:
    
    For eg. [3,4,3]

    - Divide the input string in two parts of length 3,4 and 3. 6686787825 becomes 668, 6787, 825

    - Fetch the values for each partition from the hash and store it in an array. 
    
    [[keys values for 668], [key values for 6787], [key values for 825]]
    
    [["act", "bat", "cat"], ["boot", "coot"], ["mus", "nus", "our"]]

    - If values for each partition exists then combine the arrays to append to the output array.
    
    ["act,boot,mus", "act,boot,nus", "act,boot,our", "act,coot,mus", "act,coot,nus", "act,coot,our", "bat,boot,mus", "bat,boot,nus", "bat,boot,our", "bat,coot,mus", "bat,coot,nus", "bat,coot,our", "cat,boot,mus", "cat,boot,nus", "cat,boot,our", "cat,coot,mus", "cat,coot,nus", "cat,coot,our"]
    
    - Keep pushing into the array till the last partition is processed.
  


