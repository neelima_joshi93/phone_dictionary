require_relative 'configuration'

class Phone
	attr_accessor :dictionary, :partitions, :output

	def initialize 
        @dictionary = {}
        @output = []
        @partitions = []
        prepare_dictionary_hash
        generate_partitions
	end


	def convert
		puts "Enter your input"
	    input = gets.chomp
	    output = generate_combinations(input)
	    puts "OUTPUT: #{output}"
	end


	def generate_combinations input
		validation, input = validate_input(input)
		unless validation == "passed"
			return validation             #return string with validation msg
			exit
		end
		partitions.each do |part|
			combinations = []
			phone = input.dup
			part.each do |p|
				phn = phone.slice!(0,p)
				break unless dictionary[phn]
				combinations << dictionary[phn] 
			end
			combine(combinations) if combinations.size == part.size   
		end
        return output.flatten             #return array of valid words
	end



	private

    #creates a hash with words as values and corresponding number mapping as key. 
    #example {"668" => ["mot","not", "oot"] , ...}
	def prepare_dictionary_hash
		File.readlines('dictionary.txt').each do |line|
			word = line.chomp.downcase
			number = word.chars.map { |char| Configuration::MAPPING[char]}.join()
			dictionary[number] ||=  []
			dictionary[number] << word
		end
	end


    # create an array depicting in how many ways can a string of length n be divided with min n chars in each partition
	def generate_partitions
		min = Configuration::MIN_PARTITION_LENGTH
		n = Configuration::INPUT_LENGTH
        m = n/min 
        arr = Array.new(m){min}        
        loop do
            break if arr.size == 1
            arr << arr.pop + 1  while arr.sum != n
            partitions.push(arr.dup)
            arr.pop
            arr << arr.pop + 1       
            mins_to_append = (n - arr.sum)/min
            mins_to_append.times { arr.push min} 
        end
        partitions << [n]
	end 


    #return input and validation msg
	def validate_input input 
		return "passed", input if input.match? /\A[2-9]{#{Configuration::INPUT_LENGTH}}\z/
	    return "Input size should be #{Configuration::INPUT_LENGTH}\nInput should only contain numbers ( 0's and 1's are not allowed)"
	end


    #combine the words in array using comma
	def combine combinations
		array = combinations.dup
		valid_words = []
		if array.size > 1
			array.size.times do |i|
				if valid_words.empty?
					valid_words = array[i].map {|a| array[i+1].map { |b| a+','+b  }}
				else
					valid_words = valid_words.flatten.map {|a| array[i+1].map { |b| a+','+b  } } unless array[i+1].nil?
				end
			end
		else
			valid_words << array
		end
		output << valid_words.flatten
	end

end