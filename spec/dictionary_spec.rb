require './phone.rb'

RSpec.describe Phone do

  it 'returns array of valid words for phone 6686787825' do
    input = "6686787825"
    output= Phone.new.generate_combinations input
    expect(output).to be_a(Array)
    expect(output).to match_array(["mot,opts,taj", "mot,opus,taj", "mot,orts,taj", "not,opts,taj", "not,opus,taj", "not,orts,taj",
     "oot,opts,taj", "oot,opus,taj", "oot,orts,taj", "mot,opt,puck", "mot,opt,ruck", "mot,opt,suck", "mot,ort,puck", "mot,ort,ruck", 
     "mot,ort,suck", "not,opt,puck", "not,opt,ruck", "not,opt,suck", "not,ort,puck", "not,ort,ruck", "not,ort,suck", "oot,opt,puck", 
     "oot,opt,ruck", "oot,opt,suck", "oot,ort,puck", "oot,ort,ruck", "oot,ort,suck", "noun,struck", "onto,struck", "noun,pup,taj", 
     "noun,pur,taj", "noun,pus,taj", "noun,sup,taj", "noun,suq,taj", "onto,pup,taj", "onto,pur,taj", "onto,pus,taj", "onto,sup,taj",
     "onto,suq,taj", "motor,truck", "motor,usual", "nouns,truck", "nouns,usual", "motortruck"])    
    expect(output.count).to eq(44)
  end

  it 'returns array of valid words for phone 2282668687' do
    input = "2282668687"
    output= Phone.new.generate_combinations input
    expect(output).to be_a(Array)
    expect(output).to match_array(["act,amounts", "act,contour", "bat,amounts", "bat,contour", "cat,amounts", "cat,contour", "act,boot,mus", 
      "act,boot,nus", "act,boot,our", "act,coot,mus", "act,coot,nus", "act,coot,our", "bat,boot,mus", "bat,boot,nus", "bat,boot,our", 
      "bat,coot,mus", "bat,coot,nus", "bat,coot,our", "cat,boot,mus", "cat,boot,nus", "cat,boot,our", "cat,coot,mus", "cat,coot,nus",
      "cat,coot,our", "act,boo,tots", "act,boo,tour", "act,con,tots", "act,con,tour", "act,coo,tots", "act,coo,tour", "bat,boo,tots", 
      "bat,boo,tour", "bat,con,tots", "bat,con,tour", "bat,coo,tots", "bat,coo,tour", "cat,boo,tots", "cat,boo,tour", "cat,con,tots", 
      "cat,con,tour", "cat,coo,tots", "cat,coo,tour", "acta,mounts", "acta,mot,mus", "acta,mot,nus", "acta,mot,our", "acta,not,mus",
      "acta,not,nus", "acta,not,our", "acta,oot,mus", "acta,oot,nus", "acta,oot,our", "catamounts"])
    expect(output.count).to eq(53)
  end

    
  it 'returns array of valid words for phone 9675384397' do
    input = "9675384397"
    output= Phone.new.generate_combinations input  
    expect(output).to be_a(Array)
    expect(output).to include("wop,jet,hews").and include("wop,lev,hews").and include("wos,jet,hews").and include("wos,let,hews").and include("worldviews")
  end

  it 'returns array of valid words for phone 7722327238' do
    input = "7722327238"
    output = Phone.new.generate_combinations input  
    expect(output).to be_a(Array)
    expect(output).to include("spa,afar,aft").and include("spa,afar,bet").and include("spa,bear,bet").and include("spacecraft")
  end
  
  it 'returns empty array for 9999999999' do
    input = "9999999999"
    output = Phone.new.generate_combinations input  
    expect(output).to be_a(Array)
    expect(output.count).to eq(0)    
  end
  
  it 'returns invalid input string for phone 2282668680' do
    input = "2282668680"
    output = Phone.new.generate_combinations input
    expect(output).to be_a(String)
  end

  it 'returns invalid input string for phone 2282' do
    input = "2282"
    output = Phone.new.generate_combinations input
    expect(output).to be_a(String)
  end

  it 'returns invalid input string for phone 22826686999' do
    input = "22826686999"
    output = Phone.new.generate_combinations input
    expect(output).to be_a(String)
  end

  it 'returns invalid input string for phone 22826' do
    input = "22826"
    output = Phone.new.generate_combinations input  
    expect(output).to be_a(String)
  end
  

  it 'returns invalid input string for phone 91-6879652345' do
    input = "91-6879652345"
    output = Phone.new.generate_combinations input  
    expect(output).to be_a(String)
  end

end


# Finished in 1 minute 6.04 seconds (files took 0.11555 seconds to load)
# 9 examples, 0 failures

